﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LibPollutionformules;
using Prilogeney.Reports;
using DataFromBusinessObject;
using FastReport;
using FastReport.Export.Image;
using FastReport.Export.PdfSimple;
using System.Security.Cryptography.X509Certificates;

namespace Prilogeney
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<MyParameter> Parametres;
        private static string inFolder = @"..\..\..\in\";
        public MainWindow()
        {
            InitializeComponent();

            inFolder = Utils.FindDirectory("in");
            //outFolder = Directoty.GetParent(inFolder).FullName + "\\out";
        }
        public int E = 0;


        AirPollutionCountClass AirPollutionCount = new AirPollutionCountClass();


        //Кнопка Calcutale
        private void Button_Click(object sender, RoutedEventArgs e)
        {


            foreach (TextBox tx in InputGrid.Children.OfType<TextBox>())
            {
                if (tx.Text.ToString() == string.Empty)
                {
                    MessageBox.Show("Необходимо заполнить все данные");
                    return;
                }

            }



            AirPollutionCountClass f1 = new AirPollutionCountClass();
            f1.Velocity = Convert.ToDouble(Velocitybox.Text.ToString());

            int k = 0;
            int j = 0;
            // Вывод финальных данных на форму
            foreach (var info in GridOut.Children.OfType<TextBox>())
            {


                if (j < 5)
                {
                    if (Double.IsNaN(AirPollutionCount.Cy_ConcentrationCountOfDistanceCount(Convert.ToDouble(Ebox.Text.ToString()), Convert.ToDouble(nubox.Text.ToString()), Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(TRbox.Text.ToString()), Convert.ToDouble(Tabox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString()), Convert.ToDouble(Abox.Text.ToString()))[0, 0]))
                    {
                        MessageBox.Show("Проверьте вводимые данные");
                        return;
                    }
                    info.Text = Math.Round(AirPollutionCount.Cy_ConcentrationCountOfDistanceCount(Convert.ToDouble(Ebox.Text.ToString()), Convert.ToDouble(nubox.Text.ToString()), Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(TRbox.Text.ToString()), Convert.ToDouble(Tabox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString()), Convert.ToDouble(Abox.Text.ToString()))[k, j], 4).ToString();
                    DustAmount.Text = AirPollutionCount.M_DustAmount(Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString())).ToString();
                    DangerWindVelocity.Text = AirPollutionCount.um_DangerousWindVelocity(Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(TRbox.Text.ToString()), Convert.ToDouble(Tabox.Text.ToString())).ToString();
                    Xmfak.Text = AirPollutionCount.Xm_TorchDistance(Convert.ToDouble(Ebox.Text.ToString()), Convert.ToDouble(nubox.Text.ToString()), Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(TRbox.Text.ToString()), Convert.ToDouble(Tabox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString())).ToString();


                    if (k < 4)
                        k++;
                    else
                    {
                        j++;
                        k = 0;
                    }
                }

            }
            int o = 0;

            foreach (TextBox tx in Inside2.Children.OfType<TextBox>())
            {
                if (Double.IsNaN(AirPollutionCount.Cy_ConcentrationCountOfDistanceCount(Convert.ToDouble(Ebox.Text.ToString()), Convert.ToDouble(nubox.Text.ToString()), Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(TRbox.Text.ToString()), Convert.ToDouble(Tabox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString()), Convert.ToDouble(Abox.Text.ToString()))[0, 0]))
                {

                    Convert.ToDouble(V1box);
                    break;
                }
                if (o < 5)
                {
                    tx.Text = (Math.Round(AirPollutionCount.Cx_ConcentrationCountOfDistanceCount(Convert.ToDouble(Ebox.Text.ToString()), Convert.ToDouble(nubox.Text.ToString()), Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(TRbox.Text.ToString()), Convert.ToDouble(Tabox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString()), Convert.ToDouble(Abox.Text.ToString()))[o], 4).ToString());
                }
                if (o < 4)
                {
                    o++;
                }
                else
                {
                    o = 0;
                }
            }



        }



        private void Hbox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            e.Handled = "0123456789,".IndexOf(e.Text) < 0;
        }

        private void Ebox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "01".IndexOf(e.Text) < 0;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            foreach (TextBox tx in InputGrid.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }
            foreach (TextBox tx in GridOut.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }
            foreach (TextBox tx in Inside1.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }
            foreach (TextBox tx in Inside2.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Hbox.Text = "80";
            Dbox.Text = "6,4";
            TRbox.Text = "100";
            Tabox.Text = "30";
            Zobox.Text = "0,1";
            V1box.Text = "1198800";
            nubox.Text = "75";
            Abox.Text = "160";
            Ebox.Text = "1";
            Velocitybox.Text = "5";

            foreach (TextBox tx in Inside1.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }
            foreach (TextBox tx in Inside2.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }
            foreach (TextBox tx in GridOut.Children.OfType<TextBox>())
            {
                if (tx is TextBox)
                    (tx).Text = null;

            }


        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            CreateBusinessObject();

            // create report instance
            Report report = new Report();

            // load the existing report
            report.Load($@"{inFolder}\report.frx");

            // register the array
            report.RegisterData(Parametres, "Parametres");

            // prepare the report
            report.Prepare();

            // save prepared report

            report.SavePrepared("Prepared_Report.fpx");

            // export to image
            ImageExport image = new ImageExport();
            image.ImageFormat = ImageExportFormat.Jpeg;
            report.Export(image, "report.jpg");

            #region -- Export to PDF

            PDFSimpleExport pdfExport = new PDFSimpleExport();

            pdfExport.Export(report, "report.pdf");

            #endregion
        }
        private void CreateBusinessObject()
        {
            Parametres = new List<MyParameter>()
            {
                new MyParameter
                {
                    Velocity = AirPollutionCount.um_DangerousWindVelocity(Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(TRbox.Text.ToString()), Convert.ToDouble(Tabox.Text.ToString())).ToString(),
                    Dust = AirPollutionCount.M_DustAmount(Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString())).ToString(),
                    Distance = AirPollutionCount.Xm_TorchDistance(Convert.ToDouble(Ebox.Text.ToString()), Convert.ToDouble(nubox.Text.ToString()), Convert.ToDouble(V1box.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(TRbox.Text.ToString()), Convert.ToDouble(Tabox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString())).ToString(),
                    m1 = M1box.Text,   m2 = M2box.Text,   m3 = M3box.Text,   m4 = M4box.Text,   m5 = M5box.Text

                }
            };

        }
    }
}

