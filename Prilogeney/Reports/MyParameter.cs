﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prilogeney.Reports
{
    public class MyParameter
    {
        public string Velocity{ get; set; }

        public string Dust { get; set; }
        public string Distance { get; set; }
        public string m1 { get; set; }
        public string m2 { get; set; }
        public string m3 { get; set; }
        public string m4 { get; set; }
        public string m5 { get; set; }


        public MyParameter(string speed, string pal, string dist, string _m1, string _m2, string _m3, string _m4, string _m5)
        {
            Velocity = speed;
            Dust = pal;
            Distance = dist;
            m1 = _m1;
            m2 = _m2;
            m3 = _m3;
            m4 = _m4;
            m5 = _m5;


        }
        public MyParameter()
        {
        }
    }
}
